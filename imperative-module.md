# Imperative Modules

Idea: Define Rust modules using Rust code at compile time.

**Note**: This was an alternative idea, ultimately dropped because of its lack of clear boundary between the codegen code,
and the actual consuming crate code - which would be a big undertaking both in terms of rule formuation, and implementing
with the current Rust compiler.

A rust module or source file is processed as an imperative module using the `imperative_mod` attribute:

```rs
#![imperative_mod]

// code...
```

or

```rs
// regular source file...
#[imperative_mod]
mod imp_mod {
    // code...
}
```

This is then transformed into a regular Rust code during build.

It would be preferable for syntax support to be such that any regular Rust module can be converted into an IM just by
adding the `imperative_mod` attribute, with no other changes required.

## Benefits/Motivation

- Much more power than `macro_rules`
- Far easier to learn than procedural macros
- Would allow for far more composability of code generation functionality imported from other crates than procedural
macros
- Would utilise regular Rust type checks to prevent downstream compile errors in code which uses Imperative
Module functionality from another crate
    - Example - two ways of defining some repetitive functions:

    ```rs
    pub fn mul_a(x: usize): usize {
        x * 2
    }

    pub fn mul_b(x: u32): u32 {
        x * 2
    }
    ```

    Using imported macro - generated code could be anything:

    ```rs
    use some_crate::define_fns;
    define_fns! {
        ("mul_a", usize),
        ("mul_b", u32),
        x * 2
    }
    ```

    Using imperative module with imported code gen helper - type check tells us we will get a collection of functions:

    ```rs
    #![imperative_mod]

    use imperative_mod::RsFunction;
    use some_crate::define_fns;

    let my_fns: Vec<RsFunction> = define_fns(
        &[
            ("mul_a", usize),
            ("mul_b", u32),
        ],
        fn _placeholder_name() {
            x * 2
        }
    );

    for f in my_fns {
        pub f;
    }
    ```

An imperative module is executed at compile time as a regular rust function body, with a few key differences:
- Module item definitions (i.e. functions, types, sub-modules) can be defined in an expression position.
- Any module item expression can be written in the statement: `pub {{module_item}};`. This adds the item to the public
module members.
- Imported items must be from another crate, or another imperative module.
    - This restrictions is likely necessary as we can't run code from the consuming crate itself, because the consuming
    crate won't be evaluated/compiled until all imperative modules have been converted to regular Rust code.

## Module item expressions

Within the imperative code flow, module items are just regular values, and can be assigned to bindings, used as function
args, etc. They can be constructed using an appropriate rust function, e.g.:

```rust
#![imperative_mod]

use imperative_mod::{RsFunction, RsType, RsFuncBody};
use imperative_mod::std_types::{StdUsize, StdString};

let my_fn = RsFunction::new_basic(
    "do_it",
    StdUsize,
    &["some_str", StdString],
    RsFuncBody::from_buf("some_str.len()"),
);
let my_type = RsType::new_struct_basic("SomeType", &[("a", StdUsize), ("b", StdString)]);
```

Or they can be defined as they would look in a regular module:

```rust
#![imperative_mod]

let my_fn = fn do_it(some_str: String) -> usize {
    some_str.len()
};
let my_type = struct SomeType {
    a: usize,
    b: String,
};
```

## Literal item templates

It would be useful to be able to be able to define module items using partial literal syntax, but substituting some
values. This would probably look very similar to the [`quote!` macro](https://doc.rust-lang.org/proc_macro/macro.quote.html).

Some examples (using pseudo template syntax, not thoroughly thought-out):

```rs
// without templates
let fns = &["TypeA", "TypeB"].iter().map(|fn_name| {
    let fn_def = fn _placeholder_name() {
        println!("This func is called {}", fn_name);
    };

    fn_def.with_name(fn_name)
});

// with templates
let fns = &["TypeA", "TypeB"].iter().map(|fn_name| template! {
    fn #fn_name() {
        println!("This func is called {}", fn_name);
    }
});
```

```rs
let fields = &[
    ("a", RsString),
    ("b", RsUsize),
];

// without templates
let mut my_type = struct A;

for (field_name, field_type) in fields {
    my_type.add_basic_field(field_name, field_type);
}

// with templates
let fields = fields
    .into_iter()
    .map(|(field_name, field_type)| template! {
        #field_name: #field_type
    });

let my_type = template! {
    struct A {
        #fields
    }
}
```

## Free-standing imperative module code

Regular rust code using `imperative_module` types can be defined in a crate, without using the `#[imperative_mod]`
attribute, and then exported and used in another crate's `#[imperative_mod]`s. It might be helpful to also allow special
imperative module syntax to be used in such code, by transforming said syntax with a procedural macro.

```rs
mod a_regular_module {
    use imperative_module::{RsString, RsType, util_func};

    // No macro required
    pub fn my_helpful_imperative_mod_util_a(type_name: &str, field_b_type: RsType): RsType {
        RsType::new_simple_struct(type_name, &[("a", RsString), ("b", field_b_type)])
    }

    // Using procedural macro to transform syntax
    #[util_func]
    pub fn my_helpful_imperative_mod_util_b(type_name: &str, field_b_type: RsType): RsType {
        let t = struct _placeholder_name {
            a: String
        };

        t.set_name(type_name);
        t.add_field(("b", field_b_type));

        t
    }

    // Maybe `template` macro doesn't require `util_func` attribute
    pub fn my_helpful_imperative_mod_util_c(type_name: &str, field_b_type: RsType): RsType {
        template! {
            struct #type_name {
                a: String,
                b: #field_b_type
            }
        }
    }
}
```

## Questions

### Engine implementation method

Rust's procedural macro system would allow complete conversion of the module in question before proper compilation.
However, there's no simple way to inject the consumer-side Rust code into the procedural macro runtime.

It could require the consumer to add a step to `build.rs`, calling some `prepare_imperative_modules()` functionality.

This might work like this:

- Search for imperative modules in crate, give them each a deterministic ID (just the module path?)
- Replace IM-specific syntax (i.e. literal module items, `pub` statements) to Rust structures & code
- Create staging source file:
    - Place each IM inside a function declaration `fn mod_id_name() -> RsImpModule { }`
    - `RsImpModule::render_rust_mod()` on each returned IM to produce output token streams
    - Serialise each token stream and add to some cache file, with a lookup against the module's deterministic ID
- Compile staging source file with `rustc` to obtain token stream cache file(s)

Then, once the consuming crate's compilation has started, when an `#[imperative_module]` is encountered, the
corresponding procedural macro code will simply do the following:

- Get IM's deterministic ID
- Look up token stream in cache file(s)
- Return stream to compiler

### Macros & attributes

We need to support foreign macros & attributes in some fashion. Otherwise, basic built-in functionality (`fmt!`,
`println!` etc) would be unsupported within IM code, as well as the existing ecosystem of procedural macros, and
`#[derive()]`.

If we follow the basic idea laid out in the Engine Implemenation Method section, it seems to make sense to evaluate
macro definitions & invocations during the conversion to regular modules - as well as presenting any `pub` macros in the
output Rust module.

Macros defs/invocations which are in `pub` functions which are never executed by the IM engine will not be defined/used,
but will appear in the output function code.

### Nested imperative modules

E.g. should this be allowed:

```rs
#![imperative_mod]

#[imperative_mod]
mod a {
    #[imperative_mod]
    mod b {}
}
```

Haven't really thought about the implications of this.

### Semicolon-less items return type ambiguity

In order to support easy conversion to an imperative module (i.e. no code changes required), we need to support
declaration of items which don't end in a semicolon:

```rs
#![imperative_mod]

pub struct A {}

// Identical to:
pub struct A {};
```

This leads to an ambiguity about the type of an expression where a semicolon-less item is at the end of a code block:

```rs
#![imperative_mod]

// Is `t` of type `()` or `RsType`?
let t = {
    struct A {}
};
```
