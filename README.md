# comptime-rs

Ideas:

- Higher-level-than-AST representation and manipulation of Rust code in procedural macros.
- Compile-time machinery to allow writing procedural macro code within the consuming crate/module.

```rs
use comptime as ct;

// Expression where the returned value is a code item/items to be declared in the consuming crate
ct::emit! {
    // Analogous to proc_macro quote!, but produces comptime types
    let the_struct: ct::RsStruct = ct::quote! {
        struct Foo {
            a: usize
        }
    };

    // struct `Foo` exists in the consuming module
    return the_struct;
}

// Arbitrary comptime implementation code. Accessible from `ct::emit`/`ct::decorate` code.
ct::imp! {
    use comptime as ct;

    /// Duplicate the input function item with numeric suffices
    fn duplicate(func: ct::RsFunc): Vec<ct::RsFunc> {
        std::iter::repeat(func)
            .enumerate()
            .map(|(i, f)| f.with_name(format!("{}_{}", f.name, i)))
            .take(2)
            .collect()
    }
}

// Pass the annotated item to the specified comptime function and emit the resulting items
#[ct::decorate(duplicate)]
fn some_runtime_function(): Option<Foo> {
    Some(Foo { a: 3 })
}

// Can use ad-hoc code in decorate attribute
#[ct::decorate(|func| func.with_name("i_renamed_this"))]
fn another_function() {}
```
